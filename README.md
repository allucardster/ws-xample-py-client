Synopsis
========

Python client example for "Ratchet" websocket server. (See [ws-xample](https://gitlab.com/allucardster/ws-xample))

Requirements
============

- [Python 2.7.9](docs/install_python.md)
- [Pip >= 9.0.1](docs/install_pip.md)
- [Virtualenv >= 15.1.0](docs/install_virtualenv.md)
- [Virtualenvwrapper](docs/install_virtualenvwrapper.md)

Instalation
===========

1. Clone this repository
2. From the command-line:

```
:~$ cd ws-xample-py-client
:~$ mkvirtualenv ws-env --python=/usr/local/bin/python2.7
(ws-env):~$ pip install -r requirements.txt
```

How to run client
=================

Once the server is running, from the command line:
```
(ws-env):~$ python client.py
```

For more client examples see [autobahn-python v0.8.15 examples](https://github.com/crossbario/autobahn-python/tree/v0.8.15/examples/twisted/wamp1)

Contributors
============

- Richard Melo [Twitter](@allucardster), [Linkedin](https://co.linkedin.com/in/richardmelo)
