import sys

from twisted.python import log
from twisted.internet import reactor

from autobahn.twisted.websocket import connectWS

from autobahn.wamp1.protocol import WampClientFactory, \
                                    WampClientProtocol


class MyPubSubClientProtocol(WampClientProtocol):
   """
   Protocol class for our simple demo WAMP client.
   """
   def onSessionOpen(self):

      print "Connected!"

      def onMyEvent1(topic, event):
         print "Received event", topic, event

      self.subscribe('app/channel_xample', onMyEvent1)

      self.counter = 0

      def sendMyEvent1():
         self.counter += 1
         self.publish('app/channel_xample',
            {
               "msg": "Hello from Python!",
               "counter": self.counter
            }
         )
         reactor.callLater(2, sendMyEvent1)

      sendMyEvent1()


   def onClose(self, wasClean, code, reason):
      print "Connection closed", reason
      reactor.stop()



if __name__ == '__main__':

   log.startLogging(sys.stdout)

   if len(sys.argv) > 1:
      wsuri = sys.argv[1]
   else:
      wsuri = "ws://127.0.0.1:8025"

   print "Connecting to", wsuri

   ## our WAMP/WebSocket client
   ##
   factory = WampClientFactory(wsuri, debugWamp = False)
   factory.protocol = MyPubSubClientProtocol
   connectWS(factory)

   ## run the Twisted network reactor
   ##
   reactor.run()