Overview
========
All *nix systems have a python version installed by default. Although is possible develop apps with this python version, this is not recommended because many system process depends from this specific version. So, if in your app development you need to upgrade the python version or some dependencies, this could affect the system itself. The good practice is install the required python version for your development project and use it when run your app.

How to install python 2.7.9
===========================

### CentOS
```
:~$ sudo yum groupinstall -y 'development tools'
:~$ sudo yum install -y zlib-dev openssl-devel sqlite-devel bzip2-devel xz-libs
:~$ wget -c https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tar.xz
:~$ xz -d /home/vagrant/provision/server/Python-2.7.9.tar.xz
:~$ tar -xvf /home/vagrant/provision/server/Python-2.7.9.tar
:~$ cd Python-2.7.9
:~$ sudo ./configure --prefix=/usr/local && sudo make && sudo make altinstall
:~$ cd ..
:~$ rm -rf Python-2.7.9
```

### Ubuntu
```
:~$ sudo apt-get install build-essential
:~$ sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
:~$ wget -c https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tar.xz
:~$ xz -d /home/vagrant/provision/server/Python-2.7.9.tar.xz
:~$ tar -xvf /home/vagrant/provision/server/Python-2.7.9.tar
:~$ cd Python-2.7.9
:~$ sudo ./configure --prefix=/usr/local && sudo make && sudo make altinstall
:~$ cd ..
:~$ rm -rf Python-2.7.9
```