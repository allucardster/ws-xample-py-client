Overview
========
Pip is a tool for installing and managing Python packages.

How to install pip
==================

### CentOS
```
:~$ wget https://bootstrap.pypa.io/get-pip.py
:~$ sudo python get-pip.py
```

### Ubuntu
```
:~$ sudo apt-get install python-pip python-dev build-essential
```