Overview
========
virtualenvwrapper is a set of extensions to Ian Bicking’s virtualenv tool. (see [Virtualenvwrapper docs](http://virtualenvwrapper.readthedocs.io/en/latest/index.html))

Requirements
============
- [python](install_python.md)
- [pip](install_pip.md)
- [virtualenv](install_virtualenv.md)

How to install virtualenvwrapper
================================

### CentOS/Ubuntu
```
:~$ sudo pip install virtualenvwrapper
```

Configuration
=============

1. Create virtualenv directory in your home:

```
:~$ mkdir ~/virtenvs
```

2. Check where is the "virtualenvwrapper.sh" file in the system:

```
:~$ which virtualenvwrapper.sh
```

Example response:

```
:~$ which virtualenvwrapper.sh
/home/allucardster/.local/bin/virtualenvwrapper.sh
```

3. Add environment vars into ~/.bashrc

```
# Virtualenv Wrapper config
# where to store our virtual envs
export WORKON_HOME=$HOME/virtenvs
# where projects will reside
export PROJECT_HOME=$HOME/virtenvs/Projects-Active
# where is the virtualenvwrapper.sh
source $HOME/.local/bin/virtualenvwrapper.sh
```

4. Reload ~/.bashrc

```
:~$ . ~/.bashrc
```