Overview
========
Virtualenv is a tool to create isolated Python environments.

Requirements
============
- [python](install_python.md)
- [pip](install_pip.md)

How to install virtualenv
=========================

### CentOS/Ubuntu
```
:~$ sudo pip install virtualenv
```