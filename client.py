import sys

from twisted.python import log
from twisted.internet import reactor

from autobahn.twisted.websocket import connectWS

from autobahn.wamp1.protocol import WampClientFactory, \
                                    WampClientProtocol

class MyClientProtocol(WampClientProtocol):
   """
   Demonstrates simple Publish & Subscribe (PubSub) with Autobahn WebSockets.
   """
   channel = 'app/channel_xample'

   def messageHandler(self, topicUri, message):
      """
      Handle the messages received from server over channel topic

      Arguments:
         topicUri {string} -- channel topic URI
         message {string} -- message
      """
      print "=================================\n"
      print "Message received\n"
      print "=================================\n"
      print "channel : ", topicUri, "\n"
      print "body : ", message, "\n"
      print "=================================\n"

   def sendGrettingMessage(self):
      """
      Send a greeting message over channel topic
      """
      self.publish(self.channel, "Hello from python client!")

   def onSessionOpen(self):
      """
      Handle session open event
      """
      # Subscribe client on the channel and assign message handler
      self.subscribe(self.channel, self.messageHandler)
      # Sends a greeting message
      self.sendGrettingMessage()

if __name__ == '__main__':
   log.startLogging(sys.stdout)
   factory = WampClientFactory("ws://127.0.0.1:8025")
   factory.protocol = MyClientProtocol
   connectWS(factory)
   reactor.run()